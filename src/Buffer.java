
public class Buffer {

    private static final int BUFFER_SIZE = 5;
    private int[] bufferArray = new int[BUFFER_SIZE];
    private int counter = -1;
    private boolean BUFFER_FULL = false;
    private boolean BUFFER_EMPTY = true;

    public synchronized void put(int value) throws InterruptedException {
        if (counter >= (BUFFER_SIZE - 1)) {
            System.out.println("Buffer is full.. Wait, Thread ID: "
                    + Thread.currentThread().getId());
            BUFFER_FULL = true;
            wait();
        }
        counter++;
        bufferArray[counter] = value;
        System.out.println("put : bufferArray[" + counter + "] = " + value);

        if (BUFFER_EMPTY) {
            BUFFER_EMPTY = false;
            notify();
        }
    }

    public synchronized int get() throws InterruptedException {
        if (counter < 0) {
            System.out.println("Buffer is empty.. Wait, Thread ID: "
                    + Thread.currentThread().getId());
            BUFFER_EMPTY = true;
            wait();
        }
        int value = bufferArray[counter];
        System.out.println("get : bufferArray[" + counter + "] = " + value);
        counter--;

        if (BUFFER_FULL) {
            BUFFER_FULL = false;
            notify();
        }
        return value;
    }
}
