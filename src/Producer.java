
import java.util.Random;

public class Producer implements Runnable {

    Buffer buffer;

    public Producer(Buffer buffer) {
        this.buffer = buffer;
    }
    Random rnd = new Random();

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            try {
                buffer.put(rnd.nextInt(100));
            } catch (InterruptedException e) {
            }
        }
    }
}
