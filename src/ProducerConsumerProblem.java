
/**
 *
 * @author promise7091
 */
public class ProducerConsumerProblem {

    public static void main(String[] args) {
        Buffer boundedBuffer = new Buffer();

        new Thread(new Producer(boundedBuffer)).start();
        new Thread(new Consumer(boundedBuffer)).start();
    }
}
